.. include:: ___header.rst

.. _Reporting queries menu:

*****************************
Reporting > Queries menu
*****************************

The reports you can generate to manage the day-to-day activities (see :ref:`Reporting reporting menu`). The reports are based on queries that can use any information available inside the database such as :term:`Contract`, :term:`Work stream`, :term:`Action`, etc.

================================================
List of queries generating reports
================================================

.. sidebar:: Manage a :term:`Query`

	* **Create** a Query: https://www.stratexapp.com/Query/Create
	* **Manage** the queries: https://www.stratexapp.com/Query

|reporting_queries_list.png|


.. _Create queries:

========================
Create a query
========================

Description of the fields defining a report: 

* Short Name: Descriptive name of the query i.e. "Outstanding activities (weekly view)" 
* Query Definition: SQL statement for the query 
* Target Page: The page that will pop up if a line in the query result is selected. i.e. Activity

|reporting_query_create.png|
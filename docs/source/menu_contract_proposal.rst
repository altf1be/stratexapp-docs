.. include:: ___header.rst

********************************************
Contract > Proposal
********************************************

A :term:`Proposal` is the first step before a :term:`Contract`. It represents the description of the services you will offer under a contract. See https://www.stratexapp.com/Proposal

.. hint:: The data model of a Proposal is described here https://www.stratexapp.com/help/Proposal.htm

.. sidebar:: Manage a :term:`Proposal`

	* **Create** a Proposal: https://www.stratexapp.com/Proposal/Create
	* **Manage** the Proposals: https://www.stratexapp.com/Proposal

.. include:: __17_Create_Contract_Proposal.rst


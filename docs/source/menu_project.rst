.. include:: ___header.rst

.. _Project menu:

########################
Project menu
########################


The Project menu allows you to setup several high-level artefacts of your :term:`Project`, :term:`Work stream`, :term:`Work package`, :term:`Risk`, :term:`Issue` and :term:`Action`

.. sidebar:: Project menu bar

	Manage a :term:`Project`, :term:`Work stream`, :term:`Work package`, :term:`Risk`, :term:`Issue` and :term:`Action`
	
	|project_menu-data_model.png|

|Project_menu.png|

.. include:: ___header.rst

.. _Activity menu:

***************
View > Activity
***************

.. View > Activity

An :term:`Activity` is *a process, function or task that occurs over time, has recognizable results and is managed. It is usually defined as part of a process or plan.*

The activities are linked to a :term:`Work package` they should *result in specific outputs, which are the products, services  or attributes resulting from those activities and linked to the objectives. e.g. Project management plan, state of the art study, test cases and dissemination artefacts*

.. hint:: The data model of an Activity is described here https://www.stratexapp.com/help/Activity.htm

.. sidebar:: Manage an :term:`Activity`

	* **Create** an Activity: https://www.stratexapp.com/Activity/Create
	* **Manage** the Activities: https://www.stratexapp.com/Activity

.. include:: __23_Create_Project_Activity.rst

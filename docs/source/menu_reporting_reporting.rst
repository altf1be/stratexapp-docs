.. include:: ___header.rst

.. _Reporting reporting menu:

****************************
Reporting > Reporting menu
****************************

The user generates reports to manage the day-to-day activities. The generated reports are predefined and can be defined using the :ref:`Create queries`. The queries can use any information available inside the database such as :term:`Contract`, :term:`Work stream`, :term:`Action`, etc.

Those reports are visible online, can be exported in PDF or Excel formats.


|reports_list.png|

=======================================
Result of the reporting - online view
=======================================

|reporting_reporting_delivery_slippage.png|


==============================
Export of the report in Excel
==============================

Download the report in a Excel format |icon_excel.png| : |deliverables_in_slippage-weekly_view.xls|

|reporting_reporting_delivery_slippage_xls.png|

==============================
Export of the report in PDF
==============================

Download the report in a PDF format |icon_pdf.png| : |deliverables_in_slippage-weekly_view.pdf|

|reporting_reporting_delivery_slippage_pdf.png|
.. include:: ___header.rst

**********************
Project > Projects
**********************

Display the Projects you are allowed to work on. https://www.stratexapp.com/Project

|project_projects.png|

.. _Project details:

============================
Project details
============================

Access the detailed information about your Project such as the name, the non-working days, the email addresses to use to interact with the stakeholders.

.. sidebar:: Click on **Details** to access the :ref:`Project details`

	1. Open https://www.stratexapp.com/Project
	2. Click on **Details**


|project_details.png|

============================
Create a new Project
============================

.. hint:: The data model of a Project is described here https://www.stratexapp.com/help/Project.htm

.. sidebar:: Manage a :term:`Project`

	* **Create** a project: https://www.stratexapp.com/Project/Create
	* **Manage** the projects: https://www.stratexapp.com/Project

.. include:: __03_Create_Project.rst

----------

.. intentionally empty as "ERROR: Document may not end with a transition."


============================
Non-working days
============================

Set the :term:`Non-working days` and holidays for a Project. Those days will not be taken into account when displaying the planning.

.. hint:: The data model of a Non-working day is described here https://www.stratexapp.com/help/Project.htm

.. sidebar:: Manage the :term:`Non-working days` of a specified :term:`Project`
	
	1. https://www.stratexapp.com/Project
	2. Click on **Details**
	3. Click on **Non-Working days** tab

.. include:: __05_Create_Project_Non_working_days.rst

----------

.. intentionally empty as "ERROR: Document may not end with a transition."

============================
Activity type
============================

They are several types of :term:`activities` that we can produce during a Project. The activities types' list will be used when the manager will set the type of artefacts to be produced during the Project.
Those artefacts will be identified by their abbreviation in their file names. e.g. H2020-WP00-**RPT**-001 (see :ref:`naming conventions`)

.. hint:: The data model of an Activity type is described here https://www.stratexapp.com/help/Project.htm

.. sidebar:: Set the :term:`Activity type` of a specified :term:`Project`

	1. https://www.stratexapp.com/Project
	2. Click on **Details**
	3. Click on **Activity type** tab

.. include:: __07_Create_Project_Activity_Type.rst

----------

.. intentionally empty as "ERROR: Document may not end with a transition."

.. table:: Example of Activity types including their abbreviation

   ============    ===========================================================================
   Abbreviation     Meaning
   ============    ===========================================================================
   ACT     			Progress report
   MTG        		Meeting minutes
   RPT     			Report
   ACC      		Acceptance report
   EVT      		Event, Meeting, Kick-off
   ============    ===========================================================================


============================
Job scheduler
============================

:term:`Reports` can be automatically sent by email to a specified audience at their best convenience. The manager can generate a report including any data stored in the StratEx database such as :term:`Project`, :term:`Work package`, :term:`Work stream`, :term:`risk`, :term:`action`, :term:`contract`, :term:`request for offer`, :term:`proposal` ...


.. hint:: The data model of a Job scheduler is described here https://www.stratexapp.com/help/Project.htm

.. sidebar:: Set the :term:`Job scheduler` of a specified :term:`Project`

	1. https://www.stratexapp.com/Project
	2. Click on **Details**
	3. Click on **Job scheduler** tab

|project_job_scheduler.png|


============================
Risk / Issue Type
============================

They are several types of :term:`Risks` or :term:`Issues` that a manager and her team should take into account during a Project. Those risks should be identified ex-ante and trigger several questions; the nature of those risks may need a different kind of mitigation plan in order to 

The manager and her team should ask those questions to themselves when they plan to set the risk types: 

* What could potentially go wrong? 
* What could the team do (or plan to do) that would reduce the effects of these threats on the Project? 
* What potential opportunities could occur? 
* What could the team do to enhance the effects of these opportunities on the Project? 

.. hint:: The data model of a Risk type is described here https://www.stratexapp.com/help/Project.htm

.. sidebar:: Manage the :term:`Risk types` of a specified :term:`Project`

	1. https://www.stratexapp.com/Project
	2. Click on **Details**
	3. Click on **Risk / Issue type** tab


.. include:: __09_Create_Project_Risk_Issue_Types.rst


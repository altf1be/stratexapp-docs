.. include:: ___header.rst

.. _Master data Stakeholders:

****************************
Master data > Stakeholders
****************************


List of :term:`stakeholders` participating to a Project. A Stakeholder represents an organization involved in the Project. It can be your organization, a partner, a supplier or your client. 

================================
List of Stakeholders
================================

.. hint:: The data model of a Stakeholder is described here https://www.stratexapp.com/help/Stakeholder.htm

.. sidebar:: Manage :term:`Stakeholders`

	* **Create** a Stakeholder: https://www.stratexapp.com/Stakeholder/Create
	* **Manage** the Stakeholders: https://www.stratexapp.com/Stakeholder

|master_data_stakeholders.png|

================================================================
Details of a Stakeholder
================================================================

|master_data_stakeholders_details.png|

.. _Help menu:


########################
Help menu
########################

The help lists all pages for which documentation exists: https://www.stratexapp.com/Help/Topics

|help_menu|

.. |help_menu| image:: _static/img/help_menu.png
			:alt: help menu bar
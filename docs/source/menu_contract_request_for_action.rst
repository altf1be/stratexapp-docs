.. include:: ___header.rst

********************************************
Contract > Request for action
********************************************

A :term:`Request For Action` creates a request for something to stakeholder.

.. hint:: The data model of a Request for Action is described here https://www.stratexapp.com/help/Request_For_Action.htm

.. sidebar:: Manage a :term:`Request for Action`

	* **Create** a Request for Action: https://www.stratexapp.com/RequestForAction/Create
	* **Manage** the Requests for Actions: https://www.stratexapp.com/RequestForAction

|request_for_action_create.png|


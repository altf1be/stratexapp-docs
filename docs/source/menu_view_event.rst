.. include:: ___header.rst

.. View > Event

.. _Event menu:

**************
View > Event
**************

An :term:`event` *represents the meetings, workshops or any other happenings you should attend or organize in the scope of the* :term:`Work package`

.. note:: you can assign participants to those events, invite them by email and send them an :term:`iCalendar` or "iCal" invitation they could insert into their favourite calendar system

.. hint:: The data model of an Event is described here https://www.stratexapp.com/help/Event.htm

================
List the Events
================

.. sidebar:: Manage an :term:`Event`

	* **Create** an Event: https://www.stratexapp.com/Event/Create
	* **Manage** the Events: https://www.stratexapp.com/Event

|events_list.png|

----

================
Edit an event
================

|event_edit.png|
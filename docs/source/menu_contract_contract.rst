.. include:: ___header.rst

********************************************
Contract > Contract
********************************************

A :term:`Contract` represents an agreement between entities. In the case of :term:`Framework contract`, it can include child contracts (Specific Contracts belonging to a Framework Contract).

A Contract can be a Framework Contract (e.g. a H2020 contract), a Call for Tender or a Regional request for subsidies ...

.. hint:: The data model of a Contract is described here https://www.stratexapp.com/help/Contract.htm

.. sidebar:: Manage a :term:`Contract`

	* **Create** a Contract: https://www.stratexapp.com/Contract/Create
	* **Manage** the Contracts: https://www.stratexapp.com/Contract


.. include:: __15_Create_Contract_Contract.rst


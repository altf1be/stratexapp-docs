.. include:: ___header.rst

.. _Reporting word report templates menu:

***************************************
Reporting > Word report templates menu
***************************************


The word reports you can generate and share with your project' executives, teams, partners, customers or suppliers (see :ref:`Reporting word reports menu`) are customisable. The word reports are based on queries that can use any information available inside the database such as :term:`Contract`, :term:`Work stream`, :term:`Action`, etc.

================================================
Word templates list
================================================

.. sidebar:: Manage a :term:`Word template`

	* **Manage** the Word reports: https://www.stratexapp.com/MaintainReport


|reporting_word_report_templates.png|


.. _Create word template:

========================
Create a word template
========================

Description of the fields defining a Word report: 

* Short Name: Descriptive name of the query i.e. "Outstanding activities (weekly view)" 
* Query Definition: SQL statement for the query 
* Target Page: The page that will pop up if a line in the query result is selected. i.e. Activity


-----------------------------
Upload a new template
-----------------------------

|reporting_word_report_templates_00_upload.png|

--------------------------------
Set the cover page of the report
--------------------------------

The report is made of one cover page. Set the cover page you would like to be present at the beginning of the report.

|reporting_word_report_templates_01_cover.png|

-----------------------------------------
Set the sub reports composing the report
-----------------------------------------

The report is made of sub reports. Those sub reports display certain items stored in the project. Those items can be the information related to a :term:`Work Stream`, a :term:`Work package`, a :term:`Contract` etc.

.. note:: The sub reports can used in many different reports. You may use a report displaying the list of actions in a Meeting :term:`minutes` or an :term:`Activity report`

|reporting_word_report_templates_02_sub_report.png|
.. include:: ___header.rst

***********************
Project > Project risks
***********************

A :term:`Project` is subject to constant change in its business and wider environment. The risk environment (Project :term:`risk`) is constantly changing too. The Project's priorities and relative importance of risks will shift and change. Assumptions about risk have to be regularly revisited and reconsidered, for example at each end stage assessment. [PRINCE2Revealed]_

Project :term:`risk probability` is the evaluated likelihood of a particular outcome actually happening (including a consideration of the frequency with which the outcome may arise).

The :term:`risk impact` is the evaluated effect or result of a particular outcome actually happening.

.. hint:: PRINCE2 recognizes 3 types of issues: :term:`request for change`, :term:`off-specification`, and a :term:`problem/concern`. The Project manager is free to setup other types of issues depending on its business.

.. hint:: If one risk is "realized", it becomes an :term:`issue`. (See :ref:`Project issue`)

.. hint:: The data model of a risk is described here https://www.stratexapp.com/help/Risk.htm

.. sidebar:: Assign a :term:`Risk` to a :term:`Work package`

	* **Create** a Risk: https://www.stratexapp.com/Risk/Create
	
	* **Manage** the Risks: https://www.stratexapp.com/Risk

.. include:: __25_Create_Project_Risk.rst

----------

.. intentionally empty as "ERROR: Document may not end with a transition."

============================
Assign an Action to a Risk
============================


:term:`Actions` may be setup for each :term:`Risk` in order to mitigate the risk as much as possible. For example, one can setup regular reviews of an activity to ensure that this activity is duly taken care of and that the customer won't find obvious mistakes during the :term:`User Acceptance Test`.

.. hint:: The data model of a Project is described here https://www.stratexapp.com/help/Action.htm

.. sidebar:: Manage an :term:`Action` to :term:`mitigate` the :term:`Risk` realization

	1. Open https://www.stratexapp.com/Risk/
	2. Click on **Details**
	3. Click on **Actions for the risk**


.. include:: __26_Create_Project_Risk_Action.rst 

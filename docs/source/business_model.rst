Business model
==============

Our customers
-------------

Why: 

- Our customers need to report to their customers or management and use Excel and Word due to the lack of solutions on the market
- Consulting firms do use internal solutions to generate offers and manage Projects; nevertheless, the IP stored into their systems make them impossible to resell to their customers

Who: 

- Direct: self-employed, SME and large firms
- Indirect: Consulting companies who resell our solution to large firms

Sales & Marketing
-----------------

Sales Strategy: 

- Partnerships with Clusters in Europe; they support SMEs e.g. Agoria, Sirris, Impulse, FIT, ASE ...
- Partnerships with firms delivering IT & PM training, implementing the change in organizations e.g. unisys.com, teamprosource.eu
- Direct or indirect sales to B2B (StratEx branding)
- Indirect sales using customised user interface (StratEx inside)


Marketing strategy: 

- Advertise using offline (80%) and online (20%) media
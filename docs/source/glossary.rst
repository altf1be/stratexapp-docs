.. include:: ___header.rst

.. _glossary:

Glossary
===============

.. glossary::
	:sorted:

	Acceptance
		The formal act of acknowledging that the Project has met agreed acceptance criteria and thereby met the requirements of its stakeholders. [Prince2Glossary2009]_

	Action
		Actions are acts that people or stakeholders need to do to make the Project reach its objectives. Actions are not deliverables and are not meant to produce value for your Project (such as activities).
		
		See https://www.stratexapp.com/help/Action.htm

	Actions
		See :term:`action`

	Activity
		A process, function or task that occurs over time, has recognizable results and is managed. It is usually defined as part of a process or plan. [Prince2Glossary2009]_
		
		An activity results in specific outputs, which are the products, services or attributes resulting from the activities and linked to the objectives (e.g. Project management plan, state of the art study, test cases, dissemination artefacts). 
		
		See https://www.stratexapp.com/help/Activity.htm

	Activity report
		Protocols, which are :term:`Minutes`, describe the activities that have been performed during a specified period.

	Activity type
		Possible type of an :term:`activity`: Progress report, Meeting minutes, Report, Acceptance report, Event, Meeting, Kick-off

	Activities
		See :term:`Activity`

	Assurance
		All the systematic :term:`actions` necessary to provide confidence that the target (system, process, organization, programme, :term:`Project`, outcome, benefit, capability, product output, :term:`deliverable`) is appropriate. Appropriateness might be defined subjectively or objectively in different circumstances. The implication is that assurance will have a level of independence from that which is being assured. 

		See also ':term:`Project Assurance`’ and ‘:term:`Quality assurance`’. [Prince2Glossary2009]_

	Concession 
		An :term:`off-specification` that is accepted by the Project Board without corrective action. 

	Contract
		A contract represents an agreement between entities. In the case of Framework contract, it can include child contracts (Specific Contracts belonging to a Framework Contract). 
		
		See https://www.stratexapp.com/help/Contract.htm

	Contracts
		See :term:`Contract`

	Contrôler
		Le gestionnaire peut **contrôler** et supporter les équipes qu'elles soient internes, externes, consultants ou basées dans un pays étranger (offshore)

	Control
	 	The managers can **control** and support the teams whether they are interns, consultants or offshore participants to the Project

 	Convention de nommage
	 	StratEx_ propose des conventions de nommage pour faciliter la gestion documentaire pendant la durée du projet

	Naming Conventions
	Conventions
	 	StratEx_ proposes naming **conventions** to facilitate the document management during the Projects

	Corrective action
		A set of actions to resolve a threat to a plan’s tolerances or a defect in a product. 

	Deliverables
	Deliverable
		Product, Deliverable or Outcome is used to describe everything that the Project has to create or change, however physical or otherwise these may be. Results of Projects can vary enormously from physical items, such as buildings and machinery, to intangible things like culture change and public perception. [CCTA1998]_

	Events
	Event
		An Event represents the meetings, workshops or any other happenings you should attend or organize in the scope of the :term:`Work package`. 
		
		See https://www.stratexapp.com/help/Event.htm

	Follow-on action recommendations
		Recommended actions related to unfinished work, on-going issues and risks, and any other activities needed to take a product to the next phase of its life. These are summarized and included in the End Stage Report (for phased handover) and End Project Report. [Prince2Glossary2009]_

	Framework contract
		The concept of a framework agreement is that it is, essentially, an arrangement, which establishes the contractual terms which, will apply to subsequent orders made for the goods, services or works covered by the framework over the period of time during which, it is in force. [ProcurementLawyerOrganization]_
	
	iCal
		See :term:`iCalendar`

	iCalendar
		iCalendar is a computer file format which allows Internet users to send meeting requests and tasks to other Internet users, via email, or sharing files with an extension of ".ics".
		
		See http://en.wikipedia.org/wiki/ICalendar

	Problèmes majeurs
	Problème majeur
		Un terme utilisé pour couvrir un :term:`problème/préoccupation`, une requête telle qu'une :term:`Demande de changement`, une suggestion ou un :term:`hors norme` soulevé pendant le projet. les termes peuvent se référer à tout aspect lié au projet.

	Issues
	Issue
		A term used to cover any :term:`problem/concern`, query or :term:`Request for Change`, a suggestion or :term:`Off-Specification` raised during the Project. They can refer to anything related to the Project.
		
		See https://www.stratexapp.com/help/Issue.htm
	
	Job scheduler
		Graphical user interface for definition and monitoring of background executions such as the generation of a report

	Logging
		Logging menu enables the user to check the technical activities performed by StratEx to support their project i.e. send email, perform a backup

	Master data
		Master data menu contains StratEx administrative data; people access rights, stakeholders, persons, :term:`subscription` management, :term:`logging` and administrative access to the data

	Meetings
	Meeting
		In a meeting, two or more people come together to discuss one or more topics, often in a formal setting.

	Minutes
		Protocols, which are minutes or, informally, notes, are the instant written record of a meeting or hearing.

		See http://en.wikipedia.org/wiki/Minutes

	Missions
	Mission
		Details of a Work assignment which necessitates a travel, data include the destination, the other check-in and checkout ...

		See https://www.stratexapp.com/help/Mission.htm 
	
	Mitigate
		See :term:`Risk mitigation`

	Non-working days
		Days that aren't taken into account when the planning is build

	Hors norme
		il s'agit d'un type de :term:`risque` qui est **hors norme** et indique qu'une information doit être fournie lors du projet mais qui n'est pas disponible à ce jour (ou qui n'est pas prévue dans le futur). 
		Cela peut-être un produit ou service qui ne répond pas ou plus aux spécifications nécessaires à la bonne exécution du projet. [PRINCE2Revealed]_

	Off-specification
		 This is a type of :term:`risk`: Off-specifications are "something that should be provided by the Project, but currently is not provided (or is forecast not to be). This might be a missing product or a product not meeting its specification." [PRINCE2Revealed]_
	
	On Premise
		On-premises software is installed and run on computers on the premises (in the building) of the person or organization using the software, rather than at a remote facility, such as at a server farm or cloud somewhere on the Internet. 

		See http://en.wikipedia.org/wiki/On-premises_software
	
	Persons
	Person
		A Person is an individual working on the Project. It can be a member of your team or a representative from your client. 

		See https://www.stratexapp.com/help/Person.htm
		
	PMBOK
		*The PMBOK Guide identifies that subset of the Project management body of knowledge that is generally recognized as a good practice. "Generally recognized" means the knowledge and practices described are applicable to most Projects most of the time and there is a consensus about their value and usefulness. "Good practice" means there is a general agreement that the application of the knowledge, skills, tools, and techniques can enhance the chance of success over many Projects."* [PMBOK2013]_

		See also http://en.wikipedia.org/wiki/A_Guide_to_the_Project_Management_Body_of_Knowledge

	PRINCE2
		PRINCE2 (an acronym for Projects IN Controlled Environments) is a de facto process-based method for effective Project management. Used extensively by the UK Government, PRINCE2 is also widely recognized and used in the private sector, both in the UK and internationally. The PRINCE2 method is in the public domain, and offers non-proprietorial best practice guidance on Project management.
		
		See http://www.prince2.com/what-is-prince2

	Private Cloud
		Private cloud is cloud infrastructure operated solely for a single organization, whether managed internally or by a third-party, and hosted either internally or externally. 
		
		See http://en.wikipedia.org/wiki/Public_cloud#Private_cloud
	
	Problème/préoccupation
		C'est un type de :term:`risque`: Un.e problème/préoccupation est *"tout autre :term:`problème majeur` / préoccupation / requête / suggestion que le gestionnaire de projet doit résoudre ou remonter à un niveau supérieur."* [PRINCE2Revealed]_

	Problem/concern
		 This is a type of :term:`risk`: A problem/concern is "any other :term:`issue` / concern / query / suggestion that the Project Manager needs to resolve or escalate." [PRINCE2Revealed]_
	
	Procedure
		A series of :term:`actions` for a particular aspect of Project management established specifically for the :term:`Project` – for example, a risk management procedure. 

	Processes 
		You need to setup **processes** whether you are building a house, writing an answer to a call for proposal, complying with 
		Food Drugs Association (FDA) regulations. StratEx_ proposes you to follow best practices set by famous methodologies like Prince2
		Further reading [PRINCE2Revealed]_

	Processus
		Vous devez configurer vos **processus**, que vous construisiez une maison, que vous écriviez une réponse à un appel d'offres, que vous respectiez la réglementation sanitaire.
		StratEx_ vous propose de suivre les meilleurs pratiques de la célèbre méthodologie PRINCE2.
		Pour en savoir plus, lisez [PRINCE2Revealed]_
	
	Project
		A Project is a global entity containing contracts. It allows you to group the activities you have to perform. 
		
		See https://www.stratexapp.com/help/Project.htm
	
	Project Assurance 
		The Project Board’s responsibilities are to assure itself that the Project is being conducted correctly. The Project Board members each have a specific area of focus for Project Assurance, namely business assurance for the Executive, user assurance for the Senior User(s), and supplier assurance for the Senior Supplier(s). [Prince2Glossary2009]_

	Project Management Office
		abbreviated to PMO, is a group or department within a business, agency or enterprise that defines and maintains standards for Project management within the organization. [PMOWikipedia]_

	Proposal
		A proposal is the first step before a contract. It represents the description of the services you will offer under a contract. 
		
		See https://www.stratexapp.com/help/Proposal.htm
	
	Public Cloud
		A cloud is called a "public cloud" when the services are rendered over a network that is open for public use. 
		
		See http://en.wikipedia.org/wiki/Public_cloud#Public_cloud
	
	Quality assurance 
		An independent check that products will be fit for purpose or meet requirements. [Prince2Glossary2009]_

	Query
		Queries form the basis of list-based reports that you can use to draw customised reports from StratEx. 

		See :ref:`Reporting queries menu`

	Report
		Management products providing a snapshot of the status of certain aspects of the Project. [Prince2Glossary2009]_

	Reports
		See :term:`Report`

	Reporting
		An activity that leads to a time-driven report from the Project Manager to the Project Board or a team

	Request For Action
		If you need to request something to stakeholder, it can be recorded via a Request For Action. 
		
		See https://www.stratexapp.com/help/Request_For_Action.htm
	
	Demande de changement
		C'est un type de :term:`risque`: La demande de changement est une *"requête qu'il faut considéré liée à un changement dans la feuille de route de référence"*  [PRINCE2Revealed]_
	
	Request for change
		This is a type of :term:`risk`: Request for a change is a "request that a change to one of the baselined products should be considered" [PRINCE2Revealed]_
	
	Appel d'offre
		L'appel d'offre démarre par un cycle de proposition. L'appel représente la requête que vous avez reçue de la part de votre client pour préparer une proposition d'affaires.

	Request For Offer
		The Request for Offer starts the Proposal cycle. It represents that request you have received from your client to prepare a proposal. 
		
		See https://www.stratexapp.com/help/Request_For_Offer.htm
	
	Risques
	Risque
		Le risque est un événement considéré comme mal ou dommageable poru le projet, identifié sur votre projet et qui nécessite une gestion.

	Risk
		This represents any risk you may identify on your Project(s) and allows you to manage them 
		
		See https://www.stratexapp.com/help/Risk.htm
	
	Risk impact
		The risk impact is the evaluated effect or result of a particular outcome actually happening. [PRINCE2Revealed]_

	Risk mitigation
		Risk mitigation planning is the process of developing options and actions to enhance opportunities and reduce threats to project objectives [PMBOK2013]_

	Risk probability
		A Project risk probability is the evaluated likelihood of a particular outcome actually happening (including a consideration of the frequency with which the outcome may arise). [PRINCE2Revealed]_
	
	Risk types
		:term:`PRINCE2` recognizes 3 types of issues: :term:`request for change`, :term:`off-specification`, and a :term:`problem/concern`. [PRINCE2Revealed]_
	
	Risks
		Every Project is subject to constant change in its business and wider environment. The risk environment (Project risk) is constantly changing too. The Project's priorities and relative importance of risks will shift and change. Assumptions about risk have to be regularly revisited and reconsidered, for example at each end stage assessment. [PRINCE2Revealed]_
	
	SaaS
		Software as a service is a software licensing and delivery model in which software is licensed on a :term:`subscription` basis and is centrally hosted. 
		
		See http://en.wikipedia.org/wiki/Software_as_a_service
	
	Shared
		Every member of the team is aware of the status of the Project, its deadlines, risks and issues associated
	
	partagées
		Chaque membre de l'équipe est au courant du statut du projet, des dates de livraisons, des risques et des :term:`problèmes majeurs`

	Stakeholders
	Stakeholder
		A Stakeholder represents an organization involved in the Project. It can be your organization, a partner, a supplier or your client. 
		
		See https://www.stratexapp.com/help/Stakeholder.htm

	Subscriptions
	Subscription
		StratEx comes with several business models depending on the customer needs; the user pays periodically (monthly or yearly) the use or access of StratEx. A free version of StratEx is available on top of the "Standard", "Professional" or "Enterprise" versions

		See https://www.stratexapp.com/index.html#pricing-page  

	User Acceptance Test
		User acceptance testing (UAT) consists of a process of verifying that a solution works for the user. [AcceptanceTestingWikipedia]_ It is not system testing (ensuring software does not crash and meets documented requirements), but rather is there to ensure that the solution will work for the user i.e. test the user accepts the solution (software vendors often refer to as Beta testing).

	Word template
		A pre-developed page layout describing a certain type of report such as Meeting :term:`Minutes`

		See :ref:`Reporting word report templates menu`


	Structure de répartition du travail
		En gestion de projet et les systèmes d'ingénierie, il s'agit d'une décomposition des :term:`livrables` d'un projet en composants plus petits tels que des produits, des données, des services ou toute combinaison de ceux-ci. [WorkBreakdownStructureWikipedia]_
		
	Work breakdown structure
		In Project management and systems engineering, is a deliverable-oriented decomposition of a Project into smaller components such as  a product, data, service, or any combination thereof. [WorkBreakdownStructureWikipedia]_

	Work packages
	Work package
		A Work package is a building block of the work breakdown structure that allows the Project management to define the steps necessary for completion of the work. (e.g. Program management, Production Control Software, Testing, Deployment, Dissemination). A Work package belongs to a Work stream. 
		
		See https://www.stratexapp.com/help/Workpackage.htm

	Work stream
		A Work stream defines high-level Project steps. A Work stream belongs to a contract. 
		
		See https://www.stratexapp.com/help/Workstream.htm

	Work streams
		See :term:`Work stream`

	Action (fr)
		Les actions sont des actes que les parties prenantes du projet doivent exécuter pour que le projet atteigne ses objectifs. Les actions ne sont pas des :term:`livrables` et ne sont pas censées générer de la valeur pour votre projet (telles que des :term:`activités`).
		
		See https://www.stratexapp.com/help/Action.htm

	Activités
		Un processus, une fonction ou une tâche qui se produit au fil du temps, a des résultats reconnaissables et est géré. 
		Il est généralement défini comme faisant partie d'un processus ou d'un plan. [Prince2Glossary2009]_
		Une activité produit des résultats spécifiques, qui sont: des produits, des services ou attributs résultant des activités et liés aux objectifs.
		Par exemple: un plan de gestion de projet,une étude de l’état de l'art, des cas de test, des ressources à disséminer.
		See https://www.stratexapp.com/help/Activity.htm

	Livrables
	Livrable
		Un livrable est un produit, un document ou tout résultat  qui sert à décrire tout ce qu'un projet doit changer ou créer, cela peut être un élément physique.

		Les résultats des projets varient énormément, ils peuvent être des objets physiques comme des bâtiments ou des machines, des choses intangibles comme un changement de culture ou la perception du public. [CCTA1998]_

	Événements
	Événement
		Un événement représente les réunions, les ateliers ou tout autre actualité un participant au projet doit participer ou organiser dans le périmètre d'un  you should attend or organize in the scope of the :term:`Work package`. 
		
		See https://www.stratexapp.com/help/Event.htm

	Lot de travail
		Un lot de travail ou :term:`Work package` est un élément constitutif de la :term:`structure de répartition du travail` ou :term:`Work breakdown structure` qui permet au gestionnaire de projet  de définir les étapes nécessaires à l'achèvement des travaux. 
		
		par exemple: Gestion de programme, Logiciel de contrôle de production, Test, Déploiement, Marketer un projet. 
		
		Un lot de travail dépend d'un :term:`flux de travail` ou term:`Work stream`.

	Flux de travail
		Un flux de travail ou :term:`Work stream` définit les étapes d'un projet à un très haut niveau. Un flux de travail fait partie d'un contrat. 

		See https://www.stratexapp.com/help/Workstream.htm

	Service Order
		A **Service Order** means an order for Services served by the CUSTOMER on the CONTRACTOR in accordance with the Ordering Procedure and the agreed format which includes the Terms of Reference and any Special Terms.

	Commandes de services
		Une **Commande de services** est signifiée par le CLIENT au CONTRACTANT conformément à la procédure de commande au format convenu, qui comprend le mandat et les conditions particulières de la collaboration.

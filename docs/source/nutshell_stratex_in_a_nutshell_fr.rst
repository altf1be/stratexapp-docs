.. include:: ___header.rst

########################
StratEx en bref
########################

StratEx_ qui signifie **Ex**\ écution **Stra**\ tégie (en français), est une application Web qui permet aux gestionnaires de projets et managers de :term:`contrôler` la livraison de projets en utilisant les :term:`processus` mis en place par les clients tout en imposant des :term:`conventions` :term:`partagées` par tous les membres de l'équipe.

StratEx adresse, à minima, les besoins suivants:

* Gestion de contrat: 
	* Comment prouver que la demande de paiement d'un service est légitime? 
	* Détient-on les rapports d'acceptance et d'activité pour supporter les demandes de nos sous-contractants?
* Gestion documentaire: 
	* Les PMO connaissent-ils le statut des :term:`livrables`?
	* Où donc les collaborateurs et sous-contractants ont-ils.elles stocké les documents du projet?
* Gestion de projet: 
	* Est-ce que les équipes sont au courant des dates de livraison?
	* Vos collaborateurs.trices respectent-ils.elles les méthodes et standards définis par votre client?

********************
Customer Problem
********************

In 2006, a huge Project for the European Commission starts. It implies 30 Countries willing to exchange sensitive data to protect the citizens; 10 consulting firms have to deliver: business & technical requirements, the software, the support, the quality assurance.

Our job: ensure a smooth quality assurance but none solutions did fit our needs: assure the quality of 9.000 deliverables over 7 years.

We decided to develop our solution: StratEx_

********************
Products & Services
********************

* StratEx is proposed as a :term:`Public Cloud`, :term:`Private Cloud` or an :term:`On Premise` Web application.

|public_private_on-premise.png|


* StratEx is a perfect fit with its co-opetitors Microsoft Project, CA Clarity, Oracle Primavera EPPM and Microsoft SharePoint.
* StratEx integrate seamlessly  with Document management systems like MS SharePoint, EMC Documentum, HP Autonomy
* Our users can store their data on our servers or keep their files on Microsoft One Drive, DropBox, Google Drive their NAS

|coopetition.png|

********************************************************
Public SaaS, Private SaaS and On-Premise application
********************************************************

Due to the aspect of our SaaS solution, customers can be located anywhere in the world. 

We are focusing primarily on European based self-employed up to large firms whatever their size. That's a B2B business even if Citizens may use StratEx.

We target (A) Firms requesting grants and subsidies, (B) Firms who need to control their deliveries for legal, contractual reasons and (C) Firms delegating their PM.

********************
Business model
********************

- :term:`Public Cloud` solution (also named :term:`SaaS`) is based on subscription from 20 to 40 EUR per month per user
- :term:`Private Cloud` solution is based on setup cost + subscription from 20 to 40 EUR per month per user
- :term:`On Premise` solution is priced based on licensing fees, consulting services and three-year maintenance contracts

****************************************
Avantage compétitif de StratEx
****************************************

- Focalisation sur le contrat
- Propose des conventions de nommage pour assurer une gestion documentaire fluide et cohérente
- Propose une structure de répertoire pour le stockage de fichiers qui n'implique pas de coûts élevés de personnalisation auprès du client
- Propose des centaines de modèles de documents utilisables tels quels lors des projets
- Fournir une gestion de projet et de programme en temps réel
- Génère les rapports, le principal défi d'un gestionnaire de projet, d'un membre d'équipe et du conseil de direction

********************
Modèle de données
********************

Voici la vue hiérarchique du modèle de données de StratEx, le modèle décrit les liens entre les ressources produites pendant la vie d'un projet tels que: 

* Un :term:`Flux de travail` ou :term:`Work streams`
* :term:`Lot de travail` ou :term:`Work packages`
* Activités ou :term:`Activities`
* Équipe ou Team
* Budget
* :term:`Convention de nommage` ou :term:`Naming conventions`
* :term:`Risques` ou :term:`Risks`
* :term:`Problème majeur` ou :term:`Issues`
* :term:`Actions`
* :term:`Missions`
* :term:`Événements` ou :term:`Events`

|stratex_data_model.png|

********************
L'équipe du projet
********************

`Abdelkrim Boujraf`_ (Sales & Marketing at `ALT F1`_), `Rudolf de Schipper`_ (Delivery Lead Belgium and International Institutions at Unisys), `Sven Vandormael`_ (Project Manager at Unisys)

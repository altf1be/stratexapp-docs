.. include:: ___header.rst

.. _View menu:

########################
View menu
########################

The View menu allows you to manage the lowest level of information you may manage in a project. 

.. sidebar:: View menu bar

	Manage an :term:`Activity`, an :term:`Event` and a :term:`Mission`
	
	|view_menu-data_model.png|

|view_menu.png|


* An :term:`Activity` is *a process, function or task that occurs over time, has recognizable results and is managed. It is usually defined as part of a process or plan.*. See :ref:`Activity menu`

* An :term:`Event` *represents the meetings, workshops or any other happenings you should attend or organize in the scope of the* :term:`Work package`. See :ref:`Event menu`

* A :term:`Mission` is a detailed Work assignment which necessitates a travel. The mission information includes the destination, the other check-in and checkout. :term:`Missions` are entered in a similar way as for :term:`activities`. See :ref:`Mission menu`
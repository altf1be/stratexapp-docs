.. include:: ___header.rst

**********************
Project > Work streams
**********************

Each contract is made of 1 to several :term:`Work streams` (WS); a Work stream defines high-level Project steps (e.g. Answering to the call for tender, Participate to the H2020 Project, Provide long term support after the participation to H2020 Project)


.. hint:: The data model of a Work stream is described here https://www.stratexapp.com/help/Workstream.htm

.. sidebar:: Manage a :term:`Work stream` for a specific :term:`Project`

	* **Create** a Work stream: https://www.stratexapp.com/Workstream/Create
	
	* **Manage** the Work streams: https://www.stratexapp.com/Workstream/


.. include:: __19_Create_Project_Workstream.rst


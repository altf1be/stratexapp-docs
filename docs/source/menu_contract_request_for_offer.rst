.. include:: ___header.rst

********************************************
Contract > Request for Offer 
********************************************

The :term:`Request for Offer` starts the :term:`Proposal` cycle. It represents that request you have received from your client to prepare a proposal.

.. hint:: The data model of a Request for Offer is described here https://www.stratexapp.com/help/Request_For_Offer.htm

.. sidebar:: Manage a :term:`Request for offer`

	* **Create** a Request for offer: https://www.stratexapp.com/RequestForOffer/Create
	* **Manage** the Requests for offers: https://www.stratexapp.com/RequestForOffer

.. include:: __11_Create_Contract_Request_for_offer.rst

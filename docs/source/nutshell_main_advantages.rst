############################
Main advantages of StratEx
############################

**Better global Overview on the Project**: The StratEx provides a comprehensive and global picture of the Project from the deliverables as well as the financial/contractual point of view.

**Efficiency to maintain the central repository**: One single place to store information
Increased information reliability: In addition, the automatically generated Monthly Progress Report or Activity Report is quick and efficient. Thanks to StratEx our reports are less error-prone and therefore this ensures quality as well as efficiency in the process of report drafting and delivery.

**User friendliness and accessibility over the Internet, anywhere, anytime**: As the StratEx is easily accessible over the Internet, any authorised team member or business partner can connect to this tool to enter or review information on the deliverable(s) under his/her responsibility. This provides cohesion and consistency. Even consultants working remotely can access and use the tool to enter information, follow their planning and update their tasks based on the work performed.

**Secure and Protected Access**: Only authorised Unisys members or business partners are granted access to this tool, which is hosted in Unisys protected servers’ environment.

**Reinforced responsibility at deliverable owner level**: With StratEx being accessible from the Intranet, team members are responsible for their own part of the work and they know best what they are handling themselves. This means that there is a better coverage of persons’ activities on the Project and this also ensures that information stored in the database is complete and up-to-date.

**Naming Convention reinforced by the use of the StratEx**: The StratEx has also reinforced our Deliverable Classification/Organization, and Naming Convention. How? Each time a document is delivered or attached to an email, an entry is made in the StratEx, which automatically provides a unique identifier to name our documents. This also means that every performed deliverables has a unique tracking number.

**A better-organised Project, through better structured repositories**: Now that it is easier to record data in the StratEx, the team is no longer burdened by administrative processes and editing deliverables according to the naming convention. This is a guarantee for a more reliable and consistent information in our activities reports but also in our repositories when looking for information. As documents are correctly named, they are easier to trace, which leads to a better organised Project.

**Extracting information is easy with StratEx**: The tool is extremely user friendly when it comes to filtering information. Out of the database, you may select information as shown in the search on all Specific Contracts from the start of the Framework Contract and export them into various formats (XLS, PDF and Word). Having already used the StratEx during the course of other major Projects, our team is already familiar using the tool that supports and facilitates our reporting activities for our services.

**Document Management Storage**: With StratEx your document may be stored as a hyperlink together with a recorded entry. A very convenient and efficient way to find your documents back.
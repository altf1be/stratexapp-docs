.. include:: ___header.rst


###################################
Reporting and monitoring solution
###################################

StratEx has great functionalities for filtering and reporting information. This functionality is available for any screen as shown in the introduction but also you may set up queries in order to customise specific reports cross matching information available from the StratEx database.

|reports_list.png|

Reports are easy to extract thanks to the filtering functionalities as well as the export feature into XLS, PDF or Word.

|report_xls_outstanding_activities_weekly_view.png|

We recommend that you take a look at the |full_project_progress_report.pdf|, to view how StratEx may display information into a formal report; this document will include the SLA reporting as well.

|report_create_word.png|

For closer monitoring purposes on the Project, StratEx is able to generate reports automatically on a daily/weekly/monthly schedule. Each assignee will receive a reminder for a greater respect of deadlines.
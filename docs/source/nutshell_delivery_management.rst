.. include:: ___header.rst

###################################
Delivery management
###################################


**************************************
“Home” your personal to-do-list
**************************************

When you select the StratEx_ home page you will see, a central screen with “your” personal planned tasks; which might be: :term:`deliverables` to produce, meetings to attend (called “:term:`events`”) or :term:`missions`, business trips you might need to organise – basically, the StratEx home page is your personal dashboard on a specific Project; your to-do-list.

|home.png|

StratEx promotes efficiency as well as teamwork. Indeed in order to keep things in proper order, to compile reliable information it must be processed at the source. Each operator enters and reviews his/her own activities in the database. 

Project management Office or :abbr:`PMO (Project Management Office)` and consultants all work with and around StratEx. Once a consultant gets his/her access rights set up, he/she may access the tool directly via an Internet or Intranet hyperlink.

.. important:: At any time StratEx provides an accurate picture of all activities on the Project.

As much as the personal dashboard might be useful, it is however still necessary to be able to get a global view on the Project and to seek information from a broader source. For delivery management this may be done through the :ref:`Activity menu` or :ref:`Event menu` or :ref:`Mission menu` screens.

**************************************
Activity – Deliverables Management
**************************************

When you select the “Activity” tab, a list is populated with all the activities recorded on your Project.


|activity_list.png|

When you select the “details” option, a new screen shows up where you can edit your item.
If you prefer to create a complete new item from scratch you should select the command “create new” from the main “Activity” window.

|activity_edit.png|


.. _naming conventions:

**************************************
Naming conventions
**************************************

Activities to produce are entered in the StratEx following a so-called “naming convention” which makes it easy to identify each action performed under a specific contract or service.

It is true for Activities but also for Events, for any item recorded in StratEx which is related to a contract. Thanks to this particular and :abbr:`unique ID (unique identifier)`, it is possible to clearly identify any task performed on the Project. 

This is the basic principal under all the reporting done by StratEx. It will also provide guidance to each consultant on the way to name their deliverables/documents and will harmonize communication basis for all the consultants working on a same Project.

Each document or file thanks to this :abbr:`unique ID (unique identifier)` will be suitable for tracking. StratEx supports and provides the requested identifiers.

.. note:: Example of naming convention: *PROJ-C-SC01-REP-001-1.00_Report_April_2014.doc*

.. table:: Meaning of the naming convention

   =====    ===========================================================================
   Code     Meaning
   =====    ===========================================================================
   PROJ     Project Framework contract
   C        Common (the middle letter allows sub-grouping within a framework contract)
   SC01     Specific Contract 01
   REP      Document type (here a Report)
   001      Sequence (first report of a series)
   1.00     Version
   =====    ===========================================================================


.. note:: Activities and events screens are built up on a very similar structure; however an event may be relevant to several attendees working on various service orders. Activities may only be related to one service order. This is the main difference between the two and the reason why it was decided to use a more generic naming convention used for events.

.. important:: In order to keep track and a close crosschecking between delivery and contract, a deliverable may be marked as “contractual” when it is linked to a payment. This way a Project manager will know when he has met his contractual obligations.

.. tip:: Here is a proposal for naming conventions for your Projects

|identifiers.png|


.. note:: StratEx has been build to facilitate its usage by any team member, we hope that the tool is intuitive and self-explanatory; Please do `share your thought`_ with us at any time.
Citations
==========

.. [PMBOK2013] "A Guide to the Project Management Body of Knowledge" by Project Management Institute - 5th Ed. 2013 (589 pages) - `ISBN:978-1935589679 <http://bit.ly/PPM_books>`_

.. [ContractManagementWikipedia] http://en.wikipedia.org/wiki/Contract_management

.. [AcceptanceTestingWikipedia] http://en.wikipedia.org/wiki/Acceptance_testing#User_acceptance_testing

.. [Prince2Glossary2009] http://www.prince-officialsite.com/nmsruntime/saveasdialog.aspx?lID=1486

.. [PRINCE2Revealed] "PRINCE2 Revealed: Including How to use PRINCE2 for Small Projects" by  Colin Bentley - Taylor and Francis © 2006 (274 pages) - `ISBN:9780750666725 <http://bit.ly/PPM_books>`_

.. [ProcurementLawyerOrganization] http://www.procurementlawyers.org/pdf/PLA%20paper%20on%20Frameworks%20PDF%20Mar%2012.pdf

.. [PMOWikipedia] http://en.wikipedia.org/wiki/Project_management_office

.. [CCTA1998] "Successful Project Management with PRINCE 2" by  Central Computer & Telecommunications Agency - Stationery Office Books © 1998 (364 pages) - `ISBN:978-0113308552 <http://bit.ly/PPM_books>`_

.. [WorkBreakdownStructureWikipedia] http://en.wikipedia.org/wiki/Work_breakdown_structure
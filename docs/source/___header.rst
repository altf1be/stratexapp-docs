.. the header contains shared information by documentation pages

.. E-Mails

.. _Abdelkrim Boujraf: support@stratexapp.com
.. _Rudolf de Schipper: support@stratexapp.com
.. _Sven Vandormael: support@stratexapp.com

.. _share your thought: feedback@stratexapp.com
.. _partager vos réactions: feedback@stratexapp.com

.. Images 


.. URL Links

.. _About: https://www.stratexapp.com/Home/About
.. _ALT F1: http://www.alt-f1.be
.. _StratEx: https://www.stratexapp.com


.. Substitutions of Images

.. |stratex_data_model.png| image:: _static/img/stratex_data_model.png
			:alt: StratEx Data model

.. |public_private_on-premise.png| image:: _static/img/public_private_on-premise.png
			:alt: Public Cloud, Private Cloud, On Premise web application

.. |coopetition.png| image:: _static/img/coopetition.png
			:alt: Smooth integration with IT vendors: microsoft, documentum, oracle, computer associates

.. |top_menu.png| image:: _static/img/top_menu.png
			:alt: StratEx Top menu

.. |searching_filtering.png| image:: _static/img/searching_filtering.png
			:alt: StratEx search functionality

.. |extract_data.png| image:: _static/img/extract_data.png
			:alt: StratEx Extract data

.. |document_management_storage.png| image:: _static/img/document_management_storage.png
			:alt: StratEx Document management storage

.. |crud.png| image:: _static/img/crud.png
			:alt: StratEx Create Update Delete menu item

.. feature administration access

.. feature custom_report

.. feature events_logging


.. feature reports

.. feature contractual_management

.. |proposal_details.png| image:: _static/img/proposal_details.png
						:alt: Proposal details


.. |contract_details.png| image:: _static/img/contract_details.png
						:alt: Contract details

.. feature delivery_management


.. _About : https://www.stratexapp.com/Home/About

.. |home.png| image:: _static/img/home.png
						:alt: StratEx Home

.. |activity_list.png| image:: _static/img/activity_list.png
						:alt: StratEx Activities list

.. |activity_edit.png| image:: _static/img/activity_edit.png
						:alt: StratEx Edit an Activity

.. |identifiers.png| image:: _static/img/identifiers.png
						:alt: StratEx file identifiers

.. nutshell efficient_reporting_monitoring_tool


.. |reports_list.png| image:: _static/img/reports_list.png
						:alt: Reports list

.. |report_xls_outstanding_activities_weekly_view.png| image:: _static/img/report_xls_outstanding_activities_weekly_view.png
						:alt: Excel spread sheet extracted out of StratEx events

.. |report_create_word.png| image:: _static/img/report_create_word.png
						:alt: Reporting – Extract a formatted activity report in Microsoft Word format or PDF for a specific contract

.. |full_project_progress_report.pdf| replace:: :download:`Monthly Progress Report in PDF <_static/report/full_project_progress_report.pdf>`


.. nutshell project breakdown structure


.. |contract_sla_details.png| image:: _static/img/contract_sla_details.png
						:alt: Contract: additional information to be used in SLA monitoring


.. menu Project project_menu

.. |project_menu.png| image:: _static/img/project_menu.png
			:alt: Project menu bar
			:width: 370 px

.. |project_menu-data_model.png| image:: _static/img/project_menu-data_model.png
					:alt: Project menu: Data model
					:width: 370 px

.. |project_projects.png| image:: _static/img/project_projects.png
						:alt: StratEx Projects list

.. |project_details.png| image:: _static/img/project_details.png
						:alt: StratEx Project details
						:width: 370 px

.. |project_job_scheduler.png| image:: _static/img/project_job_scheduler.png
			:alt: Project Job scheduler
			:width: 370 px			

.. menu Contract contract_menu

.. |contract_menu.png| image:: _static/img/contract_menu.png
			:alt: contract menu bar
			:width: 370 px
			
.. |request_for_action_create.png| image:: _static/img/request_for_action_create.png
			:alt: Create a Request for Action
			:width: 370 px

.. |contract_menu-data_model.png| image:: _static/img/contract_menu-data_model.png
					:alt: Contract menu: Data model
					:width: 370 px


.. setup a new account

.. compatible_with_office_app


.. |meeting_request_outlook.png| image:: _static/img/meeting_request_outlook.png
						:alt: Meeting information converted into a meeting request on Microsoft Outlook

.. |mission_outlook.png| image:: _static/img/mission_outlook.png
						:alt: Mission information shared by email on Microsoft Outlook

.. menu_view

.. |view_menu.png| image:: _static/img/view_menu.png
					:alt: View menu bar
					:width: 370 px

.. |view_menu-data_model.png| image:: _static/img/view_menu-data_model.png
					:alt: View menu: Data model
					:width: 370 px

.. menu_view_mission

.. |missions_list.png| image:: _static/img/missions_list.png
					:alt: Missions list
					:width: 370 px

.. |mission_edit.png| image:: _static/img/mission_edit.png
					:alt: Edit a mission
					:width: 370 px


.. menu_view_events

.. |events_list.png| image:: _static/img/events_list.png
					:alt: Events list
					:width: 370 px

.. |event_edit.png| image:: _static/img/event_edit.png
						:alt: Edit an Event
						:width: 370 px


.. menu_reporting


.. |reporting_menu.png| image:: _static/img/reporting_menu.png
			:alt: Reporting menu bar
			:width: 370 px

.. menu_reporting_reporting

.. |reporting_reporting_delivery_slippage.png| image:: _static/img/reporting_reporting_delivery_slippage.png
			:alt: Delivery slippage report

.. |reporting_reporting.png| image:: _static/img/reporting_reporting.png
			:alt: List of available reports
			:width: 370 px


.. |reporting_reporting_delivery_slippage_pdf.png| image:: _static/img/reporting_reporting_delivery_slippage_pdf.png
			:alt: Delivery slippage report in PDF format

.. |reporting_reporting_delivery_slippage_xls.png| image:: _static/img/reporting_reporting_delivery_slippage_xls.png
			:alt: Delivery slippage report in XLS format

.. |deliverables_in_slippage-weekly_view.xls| replace:: :download:`Deliverables in slippage (weekly_view) - Excel <_static/report/deliverables_in_slippage-weekly_view.xls>`

.. |deliverables_in_slippage-weekly_view.pdf| replace:: :download:`Deliverables in slippage (weekly_view) - PDF <_static/report/deliverables_in_slippage-weekly_view.pdf>`


.. |icon_pdf.png| image:: _static/img/icon_pdf.png
			:alt: Icon Adobe PDF Format

.. |icon_excel.png| image:: _static/img/icon_excel.png
			:alt: Icon Excel Format

.. menu_reporting_word_reports


.. |monthly_progress_report.png| image:: _static/img/monthly_progress_report.png
			:alt: Monthly progress report

.. |PROJ-C-SC01-REP-001-1.00_Report_April_2014.doc| replace:: :download:`Activity report - Word <_static/report/PROJ-C-SC01-REP-001-1.00_Report_April_2014.doc>`


.. |PROJ-C-SC01-REP-001-1.00_Report_April_2014.pdf| replace:: :download:`Activity report - PDF <_static/report/PROJ-C-SC01-REP-001-1.00_Report_April_2014.pdf>`

.. |icon_word.png| image:: _static/img/icon_word.png
			:alt: Icon Word Format


.. menu_reporting_queries

.. |reporting_queries_list.png| image:: _static/img/reporting_queries_list.png
			:alt: Queries list, basis of the reports
			:width: 370 px

.. |reporting_query_create.png| image:: _static/img/reporting_query_create.png
			:alt: Create a new query

.. menu_word_reporting_template

.. |reporting_word_report_templates.png| image:: _static/img/reporting_word_report_templates.png
			:alt: List of Word templates
			:width: 370 px

.. |reporting_word_report_templates_00_upload.png| image:: _static/img/reporting_word_report_templates_00_upload.png
			:alt: Upload a new Word template

.. |reporting_word_report_templates_01_cover.png| image:: _static/img/reporting_word_report_templates_01_cover.png
			:alt: The cover to the word template

.. |reporting_word_report_templates_02_sub_report.png| image:: _static/img/reporting_word_report_templates_02_sub_report.png
			:alt: The parts describing the the word report

.. menu_master_data 

.. |master_data_menu.png| image:: _static/img/master_data_menu.png
			:alt: Master data menu bar

.. menu_master_data_stakeholders

.. |master_data_stakeholders.png| image:: _static/img/master_data_stakeholders.png
			:alt: Master data - List of Stakeholders
			:width: 370 px

.. |master_data_stakeholders_details.png| image:: _static/img/master_data_stakeholders_details.png
			:alt: Master data - Details of a Stakeholder

.. menu_master_data_persons

.. |master_data_persons.png| image:: _static/img/master_data_persons.png
			:alt: Master data - List of Persons
			:width: 370 px

.. |master_data_persons_details.png| image:: _static/img/master_data_persons_details.png
			:alt: Master data - Details of a Person

.. |master_data_persons_assigned_projects.png| image:: _static/img/master_data_persons_assigned_projects.png
			:alt: Master data - Projects assigned to a Person

.. Substitutions URL 

.. |SignUp| replace:: https://www.stratexapp.com/Account/CreateNewAccount

.. |StratExApp| replace:: https://www.stratexapp.com
.. include:: ___header.rst

.. _Reporting word reports menu:

******************************
Reporting > Word reports menu
******************************


During a project you need to produce reports using different formats depending on your needs: Activity report or Monthly Progress Report, Acceptance Report or even a part of a Business plan.
Those reports should fit the requirements of predefined audience such as the project' executives, your team, your partners, your suppliers or your customers.

The reports are exported in PDF or Word formats. 

.. note:: You can customise the templates which are the basis of the current reports by using the :ref:`Reporting word report templates menu`


|monthly_progress_report.png|

====================================
Export of the report in Word
====================================

Download a sample report in a Word format |icon_word.png| : |PROJ-C-SC01-REP-001-1.00_Report_April_2014.doc|

====================================
Export of the report in PDF
====================================

Download a sample report in a Word format |icon_pdf.png| : |PROJ-C-SC01-REP-001-1.00_Report_April_2014.pdf|

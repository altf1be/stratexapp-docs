.. include:: ___header.rst

############################
Project Break down Structure
############################

In the “Project” tab, the StratEx manager may build up a break down structure to split a service/task order or a Quote in first: Work streams (WS) then into Work packages (WP). Each Work stream may have several Work packages. This break down structure is very useful to organise and plan work, to insert time limits for each Work package for example. Also this organised work down structure is very helpful to sort information in view of a complete reporting.

################
SLA Monitoring
################

Information related to SLA monitoring may be stored in StratEx. It is often part of the contract information. For example the staff or the consumption of budget on a Project is added in a sub-part of the contract section. This section will be updated overtime to reflect the team working on a specific service as well as the “persons-days” sold and consumed by each resource. This information will flow on request into the SLA monitoring.

|contract_sla_details.png|

SLA can take various forms, however as a default value StratEx can already provide a range of SLA measurements; Request for actions, Risks or Issues... This topic will be further explained in the reporting section.
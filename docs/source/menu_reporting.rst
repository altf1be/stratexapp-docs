.. include:: ___header.rst

.. _Reporting menu:

#################
Reporting menu
#################

The Reporting menu allows you to manage and generate predefined reports that fit the team and the management needs. 


|reporting_menu.png|


* :ref:`Reporting Reporting menu` generates reports to manage the day-to-day activities
* :ref:`Reporting word reports menu` generates Word and PDF reports ready to be shared with a predefined audience such as the executives, the team, the partners, the suppliers or the customers
* :ref:`Reporting queries menu` gives you the opportunity to define the reports
* :ref:`Reporting word report templates menu` gives you the opportunity to define the Word reports

.. note:: All reports may display information available inside the database such as :term:`Contract`, :term:`Work stream`, :term:`Action`, etc.
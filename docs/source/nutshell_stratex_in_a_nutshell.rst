.. include:: ___header.rst

########################
StratEx in a nutshell
########################

StratEx_ which stands for **Stra**\ tegy **Ex**\ ecution, is a web application enabling Managers to :term:`control` the Project delivery using customer’ :term:`processes` enforced by :term:`conventions` :term:`shared` amongst the team members.

StratEx addressing at least the following needs:

* Contract management: 
	* How to prove that a payment claim is legitimate? 
	* Do we own acceptance and activity reports to support the claims of our subcontractors?
* Document management: 
	* Do the PMOs know the status of the deliverables? 
	* Where do his/her colleagues or subcontractors have stored them?
* Project management: 
	* Is your team aware of all the deadlines? 
	* Do your team follow the internal/customer’ methods & standards?

********************
Customer Problem
********************

In 2006, a huge Project for the European Commission starts. It implies 30 Countries willing to exchange sensitive data to protect the citizens; 10 consulting firms have to deliver: business & technical requirements, the software, the support, the quality assurance.

Our job: ensure a smooth quality assurance but none solutions did fit our needs: assure the quality of 9.000 deliverables over 7 years.

We decided to develop our solution: StratEx_

********************
Products & Services
********************

* StratEx is proposed as a :term:`Public Cloud`, :term:`Private Cloud` or an :term:`On Premise` Web application.

|public_private_on-premise.png|


* StratEx is a perfect fit with its co-opetitors Microsoft Project, CA Clarity, Oracle Primavera EPPM and Microsoft SharePoint.
* StratEx integrate seamlessly  with Document management systems like MS SharePoint, EMC Documentum, HP Autonomy
* Our users can store their data on our servers or keep their files on Microsoft One Drive, DropBox, Google Drive their NAS

|coopetition.png|

********************************************************
Public SaaS, Private SaaS and On-Premise application
********************************************************

Due to the aspect of our SaaS solution, customers can be located anywhere in the world. 

We are focusing primarily on European based self-employed up to large firms whatever their size. That's a B2B business even if Citizens may use StratEx.

We target (A) Firms requesting grants and subsidies, (B) Firms who need to control their deliveries for legal, contractual reasons and (C) Firms delegating their PM.

********************
Business model
********************

- :term:`Public Cloud` solution (also named :term:`SaaS`) is based on subscription from 20 to 40 EUR per month per user
- :term:`Private Cloud` solution is based on setup cost + subscription from 20 to 40 EUR per month per user
- :term:`On Premise` solution is priced based on licensing fees, consulting services and three-year maintenance contracts

****************************************
Competitive advantage of StratEx
****************************************

- Focus on the contract
- Propose Naming conventions to ensure a smooth Document management
- Propose Directory structures which don't imply huge, costly customisation to the customer
- Propose 100+ Document templates usable as such in any kind Projects
- Provide real time Programme and Project management
- Generate reports, biggest burden of a Project manager, to her team and management board

********************
Data model
********************

Here under the hierarchical view of the StratEx data model, it describes the links between the artefacts that are produced during a Project like Work streams, Work packages, Activities, Missions etc.


|stratex_data_model.png|

********************
The team
********************

`Abdelkrim Boujraf`_ (Sales & Marketing at `ALT F1`_), `Rudolf de Schipper`_ (Senior Project Manager at Unisys), `Sven Vandormael`_ (Project Manager at Unisys)
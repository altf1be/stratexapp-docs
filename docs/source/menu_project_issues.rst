.. include:: ___header.rst

.. _Project issue: 

************************
Project > Project Issues
************************

An :term:`Issue` is created when a :term:`Risk` is realized; an Issue is a term used to cover any :term:`problem/concern`, query, :term:`Request for Change`, suggestion or :term:`Off-Specification` raised during the Project. They can be about anything to do with the Project.	

.. hint:: The data model of an Issue is described here https://www.stratexapp.com/help/Issue.htm

.. sidebar:: Manage an :term:`Action` to cover a :term:`Risk` realization

	* **Create** an Issue: https://www.stratexapp.com/Issue/Create

	* **Manage** an Issue: https://www.stratexapp.com/Issue/

.. include:: __27_Create_Project_Issue.rst 

----------

.. intentionally empty as "ERROR: Document may not end with a transition."

============================
Assign an Action to an Issue
============================

:term:`Actions` may be setup for each :term:`Issue` to ensure that the Issue is closed in a timely manner. 

.. hint:: The data model of an Action is described here https://www.stratexapp.com/help/Action.htm

.. sidebar:: Manage an :term:`Action` to close an :term:`Issue` in a timely manner

	1. Open https://www.stratexapp.com/Issue/
	2. Click on **Details**
	3. Click on **Actions for the issue**

.. include:: __28_Create_Project_Issue_Action.rst

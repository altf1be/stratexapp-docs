.. include:: ___header.rst

###################################
Gestion de la livraison
###################################


**************************************
“Home” your personal to-do-list
**************************************

When you select the StratEx_ home page you will see, a central screen with “your” personal planned tasks; which might be: :term:`deliverables` to produce, meetings to attend (called “:term:`events`”) or :term:`missions`, business trips you might need to organise – basically, the StratEx home page is your personal dashboard on a specific Project; your to-do-list.

|home.png|

StratEx promotes efficiency as well as teamwork. Indeed in order to keep things in proper order, to compile reliable information it must be processed at the source. Each operator enters and reviews his/her own activities in the database. 

Project management Office or :abbr:`PMO (Project Management Office)` and consultants all work with and around StratEx. Once a consultant gets his/her access rights set up, he/she may access the tool directly via an Internet or Intranet hyperlink.

.. important:: At any time StratEx provides an accurate picture of all activities on the Project.

As much as the personal dashboard might be useful, it is however still necessary to be able to get a global view on the Project and to seek information from a broader source. For delivery management this may be done through the :ref:`Activity menu` or :ref:`Event menu` or :ref:`Mission menu` screens.

**************************************
Activity – Deliverables Management
**************************************

When you select the “Activity” tab, a list is populated with all the activities recorded on your Project.


|activity_list.png|

When you select the “details” option, a new screen shows up where you can edit your item.
If you prefer to create a complete new item from scratch you should select the command “create new” from the main “Activity” window.

|activity_edit.png|


.. _convention de nommage:

**************************************
Convention de nommage
**************************************

Les :term:`activités` qui sont à délivrer pendant le projet sont introduites dans StratEx en utilisant une **convention de nommage**. La convention facilite l'identification des actions exécutées pendant un contrat ou un service.

La convention est nécessaire pour les :term:`activités` mais aussi les :term:`événements` ou tout autre ressource enregistrée dans StratEx_ qui est liée au contrat. 
Grâce à l'utilisation d'un :abbr:`ID unique (identifiant unique)`, il est possible d'identifier clairement toute tâche exécutée lors du projet. 

La convention est un principe basic qui concerne tout le système de rapports générés par StratEx_.

Cette convention aide les consultants à nommer correctement les :term:`livrables` et documents et harmonise la communication entre les consultants travaillant sur un même projet.

Chaque document ou fichier est suivi grâce à un :abbr:`ID unique (identifiant unique)`. StratEx_ supporte et génère les identifiant.

.. note:: Exemple de convention de nommage: *PROJ-C-SC01-REP-001-1.00_Report_April_2014.doc*

.. table:: Description de la convention de nommage

   =====    ===========================================================================
   Code     Meaning
   =====    ===========================================================================
   PROJ     Project Framework contract
   C        Common (the middle letter allows sub-grouping within a framework contract)
   SC01     Specific Contract 01
   REP      Document type (here a Report)
   001      Sequence (first report of a series)
   1.00     Version
   =====    ===========================================================================


.. note:: 
    Les écrans décrivant les activités et les événements sont construits suivant une structure similaire;
    
    * Cependant, un événement peut être relevant pour plusieurs participants travaillant sur plusieurs :term:`commandes de services` ou :term:`service order`. 
    * Les activités ne peuvent être liées qu'à des :term:`commandes de services` ou :term:`service order`. 
    
    C'est la raison principale de la différence entre les deux et la raison pour laquelle StratEx_ utilise une convention de nommage pour les événements

.. important:: 
    Pour assurer un suivi et une vérification croisée entre les livraisons et le contrat, un livrable peut être marqué comme *"contractuel"* lorsqu'il est lié à un paiement. 
    Le gestionnaire de projet saura s'il.elle a rempli son obligation contractuelle.

.. tip:: 
    Voici une proposition de convention de nommage sur vos projets

|identifiers.png|

.. note:: 
    StratEx_ est construit pour faciliter son usage par les collaborateurs sur le projet, nous espérons que l'outil est intuitif et qu'il se passe d'explications; 
    Veuillez `partager vos réactions`_ à tout moment!
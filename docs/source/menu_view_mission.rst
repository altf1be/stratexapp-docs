.. include:: ___header.rst

.. View > Mission

.. _Mission menu:

**************
View > Mission
**************

A :term:`Mission` is a detailed Work assignment which necessitates a travel. The mission information includes the destination, the other check-in and checkout. :term:`Missions` are entered in a similar way as for :term:`activities`.

.. note:: The missions linked to the Project are disclosed to the teams. Those data might be used for budget planning and help you monitor the Calendar

.. hint:: The data model of a Mission is described here https://www.stratexapp.com/help/Mission.htm

================
List of Missions
================

.. sidebar:: Manage a :term:`Mission`

	* **Create** a Mission: https://www.stratexapp.com/Mission/Create
	* **Manage** the Missions: https://www.stratexapp.com/Mission

|missions_list.png|

----

================
Edit a Mission
================


|mission_edit.png|

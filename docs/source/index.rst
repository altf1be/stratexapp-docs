.. StratEx, PPM Easily & Affordably documentation master file, created by
   sphinx-quickstart on Wed Aug 20 22:22:18 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ___header.rst 

####################################
Welcome to StratEx's documentation
####################################

StratEx_ is a web application enabling Managers to :term:`control` the delivery using customer’ :term:`processes` enforced by :term:`conventions` :term:`shared` amongst the team members.

StratEx_ brings focused collaboration between business, consultancy and software development teams.

**********************
A bit of history
**********************

In 2006, a large Project for the European Commission starts. It implies about 30 Member States willing to exchange sensitive data to protect the citizens, find stolen items like cars, trucks or boats...

The project involves many consulting companies in order to collect the business and technical requirements, develop the applications and setup the infrastructure as well as ensure a smooth quality assurance. 

For that reason, :abbr:`e-DTM (electronic Delivery Tracking Matrix)` is born (former name of StratEx).

Our team of 25 consultants had to process more than 9000 deliverables over 7 years; meaning keep track of the delivery, quality check and storage of all those deliverables including reports, white papers, business & technical analysis, acceptance & activity reports as well as the meeting minutes.
We included the management of risks, contracts, payments, missions etc.

All those activities couldn't have been managed using neither spread sheets nor MS Access database. We decided to develop from scratch an application. 
We integrated the best practices from :term:`PRINCE2` & :term:`PMBOK` and created templates of documents that could be reused during the lifetime of the Project.

End of 2010, one member of the team left the company he was working for in order to create a Spin-off of the StratEx and called it StratEx for Strategy Execution to make it available to a bigger audience.

In 2013, we moved the software on the Cloud in order to reach a bigger audience.

The application has been used by an agency from the European Commission, several Spin-offs from the "Université Libre de Bruxelles" and we are talking with banks.

**********************
Guided tour
**********************

This guided tour walks you through the steps necessary to setup a Project until its acceptance.

.. note::

   The "Take tour" is a rather Spartan but a careful read will prove that the minimalist documentation is sufficient for most users.

.. tip::

   Buttons "Next" and "Previous" are available at the end of each page to facilitate your journey through the documentation


The main documentation for the site is organized into a couple sections:

* :ref:`site-docs`
* :ref:`user-documentation`


.. _site-docs:

**********************
In a nutshell
**********************

.. toctree::
   :maxdepth: 2

   nutshell_stratex_in_a_nutshell
   nutshell_general_concept
   nutshell_delivery_management
   nutshell_compatible_with_office_apps
   nutshell_contractual_management
   nutshell_project_breakdown_structure
   nutshell_efficient_reporting_monitoring_tool
   nutshell_main_advantages

.. _user-documentation:

====================
User Documentation
====================

.. toctree::
   :maxdepth: 2
   :numbered:

   menu_setup_new_account
   menu_project
   menu_project_projects
   menu_project_work_streams
   menu_project_work_packages
   menu_project_risks
   menu_project_issues
   menu_project_actions
   menu_contract
   menu_contract_request_for_offer
   menu_contract_proposal
   menu_contract_contract
   menu_contract_request_for_action
   menu_view
   menu_view_activity
   menu_view_event
   menu_view_mission
   menu_reporting
   menu_reporting_reporting
   menu_reporting_word_reports
   menu_reporting_queries
   menu_reporting_word_report_templates
   menu_master_data
   menu_master_data_stakeholders
   menu_master_data_persons
   menu_master_data_subscriptions
   menu_help

**********************
En bref
**********************

.. toctree::
   :maxdepth: 2
   :numbered:

   nutshell_stratex_in_a_nutshell_fr
   nutshell_delivery_management_fr

=====================
Citation and Glossary
=====================

.. toctree::
   :maxdepth: 1

   citations
   glossary

***********************
Indices and tables
***********************

* :ref:`genindex`
* :ref:`search`
* :ref:`glossary`

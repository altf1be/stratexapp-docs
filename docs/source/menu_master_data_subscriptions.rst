.. include:: ___header.rst

.. _Master data Subscriptions:

****************************
Master data Subscriptions
****************************

StratEx comes with several business models depending on the customer needs; the user pays periodically (monthly or yearly) the use or access of StratEx. 

A free version of StratEx is available on top of the "Standard", "Professional" or "Enterprise" versions.


.. note:: The detailed description of the subscriptions is described here https://www.stratexapp.com/index.html#pricing-page
.. include:: ___header.rst

***********************
Project > Work packages
***********************

A :term:`Work stream` is made 1 to several :term:`Work packages` (WP), a building block of the :term:`work breakdown structure` that allows the Project manager to define the steps necessary for completion of the work. (e.g. Program management, Production Control Software, Testing, Deployment, Dissemination).

.. note:: :term:`Risks` can be associated to the Work packages. 

.. hint:: The data model of a Work package is described here https://www.stratexapp.com/help/Workpackage.htm

.. sidebar:: Manage a :term:`Work package` for a specific :term:`Work stream`

	* **Create** a Work package: https://www.stratexapp.com/Workpackage/Create
	
	* **Manage** the Work packages: https://www.stratexapp.com/Workpackage/

.. include:: __21_Create_Project_Workpackage.rst


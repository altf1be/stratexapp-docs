.. include:: ___header.rst

.. _Contract menu:

#############
Contract menu
#############

The :term:`Contract` menu allows you to setup the contract from the :term:`Proposal` to the effective agreement of your contract: 

.. sidebar:: Contract menu bar

	Manage a :term:`Request for offer`, a :term:`Proposal`, a :term:`Contract` and a :term:`Request for Action`

	|contract_menu-data_model.png|

|contract_menu.png|


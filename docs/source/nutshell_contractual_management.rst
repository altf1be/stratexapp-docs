.. include:: ___header.rst

###################################
Contractual management
###################################

Contract management or contract administration is the management of contracts made with customers, vendors, partners, or employees. The personnel involved in Contract Administration required to negotiate, support and manage effective contracts are expensive to train and retain. 

Contract management includes negotiating the terms and conditions in contracts and ensuring compliance with the terms and conditions, as well as documenting and agreeing on any changes or amendments that may arise during its implementation or execution. 
It can be summarized as the process of systematically and efficiently managing contract creation, execution, and analysis for the purpose of maximizing financial and operational performance and minimizing risk. [ContractManagementWikipedia]_

****************************
Contractual/Budget follow-up
****************************

StratEx makes it possible to monitor all the contractual and financial aspects of a Project. In the contract section you may find a menu to enter “new request for service” followed by the creation of a proposal.

|proposal_details.png|

Staff (person-days consumption) or payment/instalment information are part of the contract entry for a better follow-up of our obligation towards our clients

Once the client has accepted the proposal, a new service order is sent to the Project manager. The data related to this new service will be entered in StratEx as shown below; actually most of the information is already in the system as the new service has been already described in the proposal. This information is copied to the next level of the process into a new contract or service order.

|contract_details.png|
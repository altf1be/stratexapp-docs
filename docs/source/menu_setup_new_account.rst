.. include:: ___header.rst

#########################
Setup a new account
#########################

To profit from StratEx you need to setup a personal account.

.. sidebar:: Setup a new account

	Learn how to setup a new account.
	
	https://www.stratexapp.com/Account/CreateNewAccount

.. include:: __71_setup_new_account.rst

#. Open the StratEx web application 
	* |StratExApp|
#. Click on Sign up to setup a new account: 
	* open the Sign up page |SignUp|
	* fill the form
	* click on Create account
#. An email is sent to the address you have mentioned earlier
#. Click on the link inside the email to activate your account 
#. Set your password to access StratEx in the future
#. That's it you can now login into StratEx and setup your Project
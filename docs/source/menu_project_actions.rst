.. include:: ___header.rst

*************************
Project > Project Actions
*************************

:term:`Actions` may be linked to one :term:`Work package`. Those actions are not term:`deliverables` and are not meant to produce value for your Project (such as :term:`activities`). They represent acts that people or stakeholders need to perform to make the Project reach its objectives. 

Such actions may be a :term:`corrective action`, a :term:`follow-on action recommendations`, a :term:`procedure` ...


.. hint:: The data model of an action is described here https://www.stratexapp.com/help/Action.htm

.. sidebar:: Manage the :term:`Actions` for a specific :term:`Work package`

	* **Create** an Action linked to a Work package: https://www.stratexapp.com/Action/Create
	* **Manage** the Actions linked to the Work packages: https://www.stratexapp.com/Action

.. include:: __29_Create_Project_Action.rst 

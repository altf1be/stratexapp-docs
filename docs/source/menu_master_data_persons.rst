.. include:: ___header.rst

.. _Master data Persons:

****************************
Master data Persons
****************************

List of :term:`Persons` participating to a Project. A Person is an individual working on the Project. It can be a member of your team or a representative from your client. 

================================
List of Persons
================================

.. hint:: The data model of a Person is described here https://www.stratexapp.com/help/Person.htm

.. sidebar:: Manage a :term:`Person`

	* **Create** a Person: https://www.stratexapp.com/Person/Create
	* **Manage** the Persons: https://www.stratexapp.com/Person

|master_data_persons.png|

================================================================
Details of a Person
================================================================

|master_data_persons_details.png|

================================================================
Assign a project to a person
================================================================

|master_data_persons_assigned_projects.png|
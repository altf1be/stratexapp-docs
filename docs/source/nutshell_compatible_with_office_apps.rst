.. include:: ___header.rst

###################################
Compatible with Office applications
###################################

StratEx is compatible with modern technology, mobile solutions and connects easily with Microsoft and Google office apps. From StratEx it is quite easily to forward information to your calendar, tasks or emails, once you synchronise your pc or laptop with your mobile phone, your outlook agenda is up-to-date with StratEx.

Usually a team member will enter a new meeting, an activity or a new mission in StratEx. As this information might be relevant to multiple participants, he/she will send this information to him/herself or to colleagues to update his/her Outlook calendar via a meeting request, a task or email carrying information extracted from StratEx.


************************************************************
Meeting request made with the Calendar of Microsoft Outlook
************************************************************

|meeting_request_outlook.png|

***********************************************************
Meeting request made with Mail client of Microsoft Outlook
***********************************************************

|mission_outlook.png|
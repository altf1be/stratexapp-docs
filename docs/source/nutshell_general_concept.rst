.. include:: ___header.rst

###################################
StratEx General concepts
###################################


The purpose of StratEx_ is to allow entering/maintaining data according to a specific structure to organise information, which is then easy to track back, to monitor, and to extract in view of providing a complete reporting to our clients.

************
Top menu
************

When you open the StratEx_ Home page you will notice two areas: the central screen with your planned duties and over - on top of the screen - a main menu, which provides access to any function available in StratEx.
The main menu is divided in many items (Project, Contract, etc.); each item provides functions to enter/review information on a specific part of the Project structure.

|top_menu.png|

.. note:: The first menu named "StratEx" is the name of your Project; Bear in mind that your screen may display another name

#. "StratEx" menu
	#. The first menu item is your personal dashboard displaying your tasks like :term:`deliverables`, :term:`meetings` and :term:`missions`
#. :ref:`View menu`
	#. :term:`Activity` menu gives access to the “deliverables” to produce.
	#. :term:`Event` stands for “meetings to attend”
	#. :term:`Mission` covers business trips/expenses management
#. :ref:`Project menu` 
	#. The user creates a :term:`work breakdown structure` for each quote or service/task order; the :term:`Project` tab makes it possible to split a contract into Work streams and Work packages
#. :ref:`Contract menu` 
	#. Follow your :term:`contracts` from a new service request to a :term:`framework contract` management via :term:`proposal` management, quotes, task or service order monitoring. Under this tab you may also manage “:term:`Request for action`”, whereas :term:`risks` and :term:`issues` may also be recorded.
#. :ref:`Reporting menu` 
	#. Generate a selection of :term:`reports`, queries, and other pre-formatted reports to be sent to a Microsoft Word document with a specific template
#. :ref:`Master data menu` 
	#. Manager the :term:`Master data` containing StratEx' administrative data; people access rights, stakeholders, database calendar (for Non-working days) and jobs
#. :ref:`Help menu`
	#. Describe in detail fields defining the artefacts like a :term:`proposal` and a :term:`request for offer` 
	

.. tip:: The search functionality is valid for any of the screens in StratEx.

***************************
Transversal functionalities
***************************

Transversal functionalities apply the same logic in any Business Object (:term:`Mission`, :term:`Event`, :term:`Risk`, :term:`Activity`, :term:`Issue`, :term:`Action`)

========================
Searching and Filtering
========================

The logical structure behind each menu item is similar; from each item you may: get a global view on a topic with searching and filtering possibilities on all items listed on this particular tab. 

|searching_filtering.png|

==================
Extracting data
==================

After filtering data you may convert them easily into a spread sheet file (XLS), a Word document or a PDF.

|extract_data.png|

==============================
Document Management Storage
==============================

In most screens in StratEx, you may upload a document attached to an entry (a deliverable, an offer, a contract...) so that whenever you are searching for information, the actual document/contract etc. is at your disposal just by clicking on the hyperlink. StratEx may be used as structured document management storage as well.


|document_management_storage.png|

==============================
Edit or delete records
==============================

From any item menu, you may edit an existing document, create a new entry or delete an existing record.

|crud.png|
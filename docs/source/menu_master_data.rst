.. include:: ___header.rst

.. _Master data menu:

########################
Master data menu
########################

The :term:`Master data` menu allows you to manage the :term:`Stakeholders` or :term:`Persons` involved in the projects you are working for; manage your :term:`Subscriptions`, 

|master_data_menu.png|


* :ref:`Master data Stakeholders` enables the user to manage the representatives of the organisations involved in the projects
* :ref:`Master data Persons` enables the user to manage the members of a project
* :ref:`Master data Subscriptions` enables the user to upgrade or downgrade the type of :term:`Subscription` required to manage the size of their project

.. * :ref:`Master data Logging` enables the user to check the technical activities performed by StratEx to support their project i.e. send email, perform a backup
.. * :ref:`Master data Admin access`
```html
  ____    _                    _     _____             _                              ____                       
 / ___|  | |_   _ __    __ _  | |_  | ____| __  __    / \     _ __    _ __           |  _ \    ___     ___   ___ 
 \___ \  | __| | '__|  / _` | | __| |  _|   \ \/ /   / _ \   | '_ \  | '_ \   _____  | | | |  / _ \   / __| / __|
  ___) | | |_  | |    | (_| | | |_  | |___   >  <   / ___ \  | |_) | | |_) | |_____| | |_| | | (_) | | (__  \__ \
 |____/   \__| |_|     \__,_|  \__| |_____| /_/\_\ /_/   \_\ | .__/  | .__/          |____/   \___/   \___| |___/
                                                             |_|     |_|                                         
```

# StratEx

`StratEx` is an easy to use, Web Project Tracking Automation tool that brings focused collaboration between business, consultancy and software development teams.

* Learn about StratEx on [StratExApp]
* Drastically improve your Project Management practice using [StratExApp] 
* Do you need support from us? [StratExApp UserVoice]
* Find us on the social networks 
	* [StratExApp Facebook]
	* [StratExApp Twitter]

# Sphinx 

## How to build a new html `rm -r build/ && make html && open build/html/index.html`
## How to build a pdf `make latexpdf && open build/latex/StratExPPMEasilyAffordably.pdf`

	rm -r build/ && make html && open build/html/index.html
	rm -r build/ 
	make latexpdf && open build/latex/StratExPPMEasilyAffordably.pdf
	curl -X POST http://readthedocs.org/build/stratexapp-docs
	make linkcheck && open build/linkcheck/output.txt 

# use multilingual capabilities including .po 

	easy_install sphinx-intl
	make gettext
	export SPHINXINTL_LANGUAGE=de,en
	sphinx-intl update -p build/locale -d source/
	sphinx-intl build -d source/
	make -e SPHINXOPTS="-D language='de'" html

## Sphinx documentation

* Sphinx (tool to build html, pdf or Qt documentation from a Restructured format)
	* http://sphinx-doc.org/index.html
* Restructured Text format
	* http://docutils.sourceforge.net/docs/user/rst/quickstart.html
	* http://docutils.sourceforge.net/docs/user/rst/quickref.html (**Best documentation when you are confident with rst format**)
	* http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt

## Pandoc

* Install [Pandoc] to convert the restructured files in word docx

[StratExApp Facebook]: https://www.facebook.com/stratexapp
[StratExApp Twitter]: https://www.twitter.com/stratexapp
[StratExApp UserVoice]: https://stratexapp.uservoice.com
[StratExApp]: https://www.stratexapp.com
[Text ASCII Art Generator]: http://patorjk.com/software/taag/#p=display&h=0&v=0&f=Ivrit&t=StratExApp-Docs
[Pandoc]: https://pandoc.org